package ar.edu.undec.adapter.data.cruds;

import ar.edu.undec.adapter.data.models.ConductorEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ConductorCRUD extends CrudRepository<ConductorEntity, Long> {
    boolean existsByDni(String dni);
    Collection<ConductorEntity> findAll();
}
