package ar.edu.undec.adapter.data.exceptions;

public class ConductorMappingException extends RuntimeException{
    public ConductorMappingException(String msg){
        super(msg);
    }
}
