package ar.edu.undec.adapter.data.mapper;

import ar.edu.undec.adapter.data.exceptions.ConductorMappingException;
import ar.edu.undec.adapter.data.models.ConductorEntity;
import models.Conductor;

public class ConductorDataMapper {
    public static Conductor dataCoreMapper(ConductorEntity conductor){
        try {
            return Conductor.getInstance(conductor.getId(),
                    conductor.getDni(),
                    conductor.getName(),
                    conductor.getLastName(),
                    conductor.getHireDate());
        }catch (Exception exception){
            throw new ConductorMappingException("error mapping from entity to core");
        }
    }

    public static ConductorEntity dataEntityMapper(Conductor conductor){
        try {
            return new ConductorEntity(conductor.getId(),
                    conductor.getDni(),
                    conductor.getName(),
                    conductor.getLastName(),
                    conductor.getHireDate());
        }catch (Exception exception){
            throw new ConductorMappingException("error mapping from core to entity");
        }
    }
}
