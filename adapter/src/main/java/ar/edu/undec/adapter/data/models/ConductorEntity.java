package ar.edu.undec.adapter.data.models;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "conductors")
public class ConductorEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String dni;
    private String name;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "hire_date")
    private LocalDate hireDate;
    public ConductorEntity(){}
    public ConductorEntity(Long id, String dni, String name, String lastName, LocalDate hireDate) {
        this.id = id;
        this.dni = dni;
        this.name = name;
        this.lastName = lastName;
        this.hireDate = hireDate;
    }

    public Long getId() {
        return id;
    }

    public String getDni() {
        return dni;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDate getHireDate() {
        return hireDate;
    }
}
