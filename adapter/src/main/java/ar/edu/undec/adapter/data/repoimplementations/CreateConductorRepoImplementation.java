package ar.edu.undec.adapter.data.repoimplementations;

import ar.edu.undec.adapter.data.cruds.ConductorCRUD;
import ar.edu.undec.adapter.data.mapper.ConductorDataMapper;
import models.Conductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import outputs.CreateConductorRepository;

@Service
public class CreateConductorRepoImplementation implements CreateConductorRepository {
    private ConductorCRUD conductorCRUD;

    @Autowired
    public CreateConductorRepoImplementation(ConductorCRUD conductorCRUD) {
        this.conductorCRUD = conductorCRUD;
    }

    @Override
    public boolean existsByDni(String dni) {
        return conductorCRUD.existsByDni(dni);
    }

    @Override
    public Long save(Conductor conductor) {
        return conductorCRUD.save(ConductorDataMapper.dataEntityMapper(conductor)).getId();
    }
}
