package ar.edu.undec.adapter.data.repoimplementations;

import ar.edu.undec.adapter.data.cruds.ConductorCRUD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import outputs.DeleteConductorRepository;

@Service
public class DeleteConductorRepoimplementation implements DeleteConductorRepository {
    private ConductorCRUD conductorCRUD;
    @Autowired
    public DeleteConductorRepoimplementation(ConductorCRUD conductorCRUD) {
        this.conductorCRUD = conductorCRUD;
    }

    @Override
    public boolean existsById(Long id) {
        return conductorCRUD.existsById(id);
    }

    @Override
    public void deleteById(Long id) {
        conductorCRUD.deleteById(id);
    }
}
