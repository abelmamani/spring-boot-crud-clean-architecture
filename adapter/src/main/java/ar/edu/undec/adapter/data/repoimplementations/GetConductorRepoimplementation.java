package ar.edu.undec.adapter.data.repoimplementations;

import ar.edu.undec.adapter.data.cruds.ConductorCRUD;
import ar.edu.undec.adapter.data.mapper.ConductorDataMapper;
import models.Conductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import outputs.GetConductorRepository;

import java.util.Optional;

@Service
public class GetConductorRepoimplementation implements GetConductorRepository {
    private ConductorCRUD conductorCRUD;
    @Autowired
    public GetConductorRepoimplementation(ConductorCRUD conductorCRUD) {
        this.conductorCRUD = conductorCRUD;
    }

    @Override
    public Optional<Conductor> findById(Long id) {
        return conductorCRUD.findById(id).map(ConductorDataMapper::dataCoreMapper);
    }
}
