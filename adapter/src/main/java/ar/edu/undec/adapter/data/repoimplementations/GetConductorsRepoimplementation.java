package ar.edu.undec.adapter.data.repoimplementations;

import ar.edu.undec.adapter.data.cruds.ConductorCRUD;
import ar.edu.undec.adapter.data.mapper.ConductorDataMapper;
import models.Conductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import outputs.GetConductorsRepository;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class GetConductorsRepoimplementation implements GetConductorsRepository {
    private ConductorCRUD conductorCRUD;
    @Autowired
    public GetConductorsRepoimplementation(ConductorCRUD conductorCRUD) {
        this.conductorCRUD = conductorCRUD;
    }

    @Override
    public Collection<Conductor> findAll() {
        return conductorCRUD.findAll().stream().map(ConductorDataMapper::dataCoreMapper).collect(Collectors.toList());
    }
}
