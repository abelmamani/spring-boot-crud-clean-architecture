package ar.edu.undec.adapter.data.repoimplementations;

import ar.edu.undec.adapter.data.cruds.ConductorCRUD;
import ar.edu.undec.adapter.data.mapper.ConductorDataMapper;
import models.Conductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import outputs.UpdateConductorRepository;

@Service
public class UpdateConductorRepoimplementation implements UpdateConductorRepository {
    private ConductorCRUD conductorCRUD;
    @Autowired
    public UpdateConductorRepoimplementation(ConductorCRUD conductorCRUD) {
        this.conductorCRUD = conductorCRUD;
    }

    @Override
    public boolean existsByDni(String dni) {
        return conductorCRUD.existsByDni(dni);
    }

    @Override
    public Conductor update(Conductor conductor) {
        return ConductorDataMapper.dataCoreMapper(conductorCRUD.save(ConductorDataMapper.dataEntityMapper(conductor)));
    }
}
