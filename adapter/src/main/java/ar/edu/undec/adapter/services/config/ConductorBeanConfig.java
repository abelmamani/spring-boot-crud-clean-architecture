package ar.edu.undec.adapter.services.config;

import inputs.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import outputs.*;
import usecases.*;

@Configuration
public class ConductorBeanConfig {
    @Bean
    public CreateConductorInput createConductorInput(CreateConductorRepository createConductorRepository){
        return new CreateConductorUseCase(createConductorRepository);
    }
    @Bean
    public GetConductorInput getConductorInput(GetConductorRepository getConductorRepository){
        return new GetConductorUseCase(getConductorRepository);
    }
    @Bean
    public GetConductorsInput getCondcutorsInput(GetConductorsRepository getConductorsRepository){
        return new GetConductorsUseCase(getConductorsRepository);
    }
    @Bean
    public DeleteConductorInput deleteConductorInput(DeleteConductorRepository deleteConductorRepository){
        return new DeleteConductorUseCase(deleteConductorRepository);
    }
    @Bean
    public UpdateconductorInput updateconductorInput(UpdateConductorRepository updateConductorRepository, GetConductorInput getConductorInput){
        return new UpdateConductorUseCase(updateConductorRepository, getConductorInput);
    }

}
