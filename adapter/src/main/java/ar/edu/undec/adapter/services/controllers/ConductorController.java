package ar.edu.undec.adapter.services.controllers;

import inputs.*;
import models.CreateConductorRequestModel;
import models.UpdateConductorRequestModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("conductors")
@CrossOrigin("*")
public class ConductorController {
    private GetConductorsInput getCondcutorsInput;
    private GetConductorInput getConductorInput;
    private CreateConductorInput createConductorInput;
    private UpdateconductorInput updateconductorInput;
    private DeleteConductorInput deleteConductorInput;
    @Autowired
    public ConductorController(GetConductorsInput getCondcutorsInput, GetConductorInput getConductorInput, CreateConductorInput createConductorInput, UpdateconductorInput updateconductorInput, DeleteConductorInput deleteConductorInput) {
        this.getCondcutorsInput = getCondcutorsInput;
        this.getConductorInput = getConductorInput;
        this.createConductorInput = createConductorInput;
        this.updateconductorInput = updateconductorInput;
        this.deleteConductorInput = deleteConductorInput;
    }
    @GetMapping
    public ResponseEntity<?> getConductors(){
        try {
            return ResponseEntity.ok(getCondcutorsInput.getConductors());
        }catch (RuntimeException exception){
            Map<String, String> response = new HashMap<>();
            response.put("message", exception.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getConductor(@PathVariable("id") Long id){
        try {
            return ResponseEntity.ok(getConductorInput.getConductor(id));
        }catch (RuntimeException exception){
            Map<String, String> response = new HashMap<>();
            response.put("message", exception.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
    }
    @PostMapping
    public ResponseEntity<?> createConductor(@RequestBody CreateConductorRequestModel createConductorRequestModel){
        try {
            return ResponseEntity.created(null).body(createConductorInput.createConductor(createConductorRequestModel));
        }catch (RuntimeException exception){
            Map<String, String> response = new HashMap<>();
            response.put("message", exception.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
    }
    @PutMapping
    public ResponseEntity<?> updateConductor(@RequestBody UpdateConductorRequestModel updateConductorRequestModel){
        try {
            return ResponseEntity.ok(updateconductorInput.updateConductor(updateConductorRequestModel));
        }catch (RuntimeException exception){
            Map<String, String> response = new HashMap<>();
            response.put("message", exception.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteConductor(@PathVariable("id") Long id){
        try {
            deleteConductorInput.deleteConductor(id);
            Map<String, String> response = new HashMap<>();
            response.put("message", "delete success");
            return ResponseEntity.ok(response);
        }catch (RuntimeException exception){
            Map<String, String> response = new HashMap<>();
            response.put("message", exception.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
    }
}
