package exceptions;

public class ConductorAlreadyExistsException extends RuntimeException{
    public ConductorAlreadyExistsException(String msg){
        super(msg);
    }
}
