package exceptions;

public class ConductorException extends RuntimeException{
    public ConductorException(String msg){
        super(msg);
    }
}
