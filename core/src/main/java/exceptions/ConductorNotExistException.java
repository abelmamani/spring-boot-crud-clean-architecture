package exceptions;

public class ConductorNotExistException extends RuntimeException{
    public ConductorNotExistException(String msg){
        super(msg);
    }
}
