package inputs;

import models.CreateConductorRequestModel;

public interface CreateConductorInput {
    Long createConductor(CreateConductorRequestModel createConductorRequestModel);
}
