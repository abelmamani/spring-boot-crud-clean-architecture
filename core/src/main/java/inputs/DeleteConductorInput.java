package inputs;

public interface DeleteConductorInput {
    void deleteConductor(Long id);
}
