package inputs;

import models.Conductor;

public interface GetConductorInput {
    Conductor getConductor(Long id);
}
