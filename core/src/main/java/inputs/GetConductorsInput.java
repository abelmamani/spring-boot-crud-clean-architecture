package inputs;

import models.Conductor;

import java.util.Collection;

public interface GetConductorsInput {
    Collection<Conductor> getConductors();
}
