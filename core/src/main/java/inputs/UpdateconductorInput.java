package inputs;

import models.Conductor;
import models.UpdateConductorRequestModel;

public interface UpdateconductorInput {
    Conductor updateConductor(UpdateConductorRequestModel updateConductorRequestModel);
}
