package models;

import exceptions.ConductorException;

import java.time.LocalDate;

public class Conductor {
    private Long id;
    private String dni;
    private String name;
    private String lastName;
    private LocalDate hireDate;

    private Conductor(Long id, String dni, String name, String lastName, LocalDate hireDate) {
        this.id = id;
        this.dni = dni;
        this.name = name;
        this.lastName = lastName;
        this.hireDate = hireDate;
    }
    public static Conductor getInstance(Long id, String dni, String name, String lastName, LocalDate hireDate) {
        if(dni == null || dni.isBlank())
            throw new ConductorException("the dni is required");
        if(name == null || name.isBlank())
            throw new ConductorException("the name is required");
        if(lastName == null || lastName.isBlank())
            throw new ConductorException("the last name is required");
        if(hireDate == null)
            throw new ConductorException("the hire date is required");
        return new Conductor(id, dni, name, lastName, hireDate);
    }

    public Long getId() {
        return id;
    }

    public String getDni() {
        return dni;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDate getHireDate() {
        return hireDate;
    }
}
