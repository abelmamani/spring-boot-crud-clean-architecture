package models;

import java.time.LocalDate;

public class CreateConductorRequestModel {
    private String dni;
    private String name;
    private String lastName;
    private LocalDate hireDate;
    private CreateConductorRequestModel(){}
    private CreateConductorRequestModel(String dni, String name, String lastName, LocalDate hireDate) {
        this.dni = dni;
        this.name = name;
        this.lastName = lastName;
        this.hireDate = hireDate;
    }

    public static CreateConductorRequestModel getInstance(String dni, String name, String lastName, LocalDate hireDate) {
        return new CreateConductorRequestModel(dni, name, lastName, hireDate);
    }

    public String getDni() {
        return dni;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDate getHireDate() {
        return hireDate;
    }
}
