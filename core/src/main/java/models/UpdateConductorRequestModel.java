package models;

import java.time.LocalDate;

public class UpdateConductorRequestModel {
    private Long id;
    private String dni;
    private String name;
    private String lastName;
    private LocalDate hireDate;
    private UpdateConductorRequestModel(){}
    private UpdateConductorRequestModel(Long id, String dni, String name, String lastName, LocalDate hireDate) {
        this.id = id;
        this.dni = dni;
        this.name = name;
        this.lastName = lastName;
        this.hireDate = hireDate;
    }

    public static UpdateConductorRequestModel getInstance(Long id, String dni, String name, String lastName, LocalDate hireDate) {
        return new UpdateConductorRequestModel(id, dni, name, lastName, hireDate);
    }

    public Long getId() {
        return id;
    }

    public String getDni() {
        return dni;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDate getHireDate() {
        return hireDate;
    }
}
