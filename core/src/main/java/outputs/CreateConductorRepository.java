package outputs;

import models.Conductor;

public interface CreateConductorRepository {
    boolean existsByDni(String dni);
    Long save(Conductor conductor);
}
