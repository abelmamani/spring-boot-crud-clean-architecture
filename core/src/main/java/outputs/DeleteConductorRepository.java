package outputs;

public interface DeleteConductorRepository {
    boolean existsById(Long id);
    void deleteById(Long id);
}
