package outputs;

import models.Conductor;

import java.util.Optional;

public interface GetConductorRepository {
    Optional<Conductor> findById(Long id);
}
