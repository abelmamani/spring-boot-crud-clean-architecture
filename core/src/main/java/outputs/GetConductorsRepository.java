package outputs;

import models.Conductor;

import java.util.Collection;

public interface GetConductorsRepository {
    Collection<Conductor> findAll();
}
