package outputs;

import models.Conductor;

public interface UpdateConductorRepository {
    boolean existsByDni(String dni);
    Conductor update(Conductor conductor);
}
