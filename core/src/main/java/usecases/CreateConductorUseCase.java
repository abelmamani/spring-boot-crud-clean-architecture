package usecases;

import exceptions.ConductorAlreadyExistsException;
import inputs.CreateConductorInput;
import models.Conductor;
import models.CreateConductorRequestModel;
import outputs.CreateConductorRepository;

public class CreateConductorUseCase implements CreateConductorInput {
    private CreateConductorRepository createConductorRepository;

    public CreateConductorUseCase(CreateConductorRepository createConductorRepository) {
        this.createConductorRepository = createConductorRepository;
    }

    @Override
    public Long createConductor(CreateConductorRequestModel createConductorRequestModel) {
        if(createConductorRepository.existsByDni(createConductorRequestModel.getDni()))
            throw new ConductorAlreadyExistsException("the conductor by dni already exists");
        Conductor conductor = Conductor.getInstance(null,
                createConductorRequestModel.getDni(),
                createConductorRequestModel.getName(),
                createConductorRequestModel.getLastName(),
                createConductorRequestModel.getHireDate());
        return createConductorRepository.save(conductor);
    }
}
