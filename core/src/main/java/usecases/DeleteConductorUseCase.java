package usecases;

import exceptions.ConductorNotExistException;
import inputs.DeleteConductorInput;
import outputs.DeleteConductorRepository;

public class DeleteConductorUseCase implements DeleteConductorInput {
    private DeleteConductorRepository deleteConductorRepository;

    public DeleteConductorUseCase(DeleteConductorRepository deleteConductorRepository) {
        this.deleteConductorRepository = deleteConductorRepository;
    }

    @Override
    public void deleteConductor(Long id) {
       if(!deleteConductorRepository.existsById(id))
           throw new ConductorNotExistException("the conductor by id not exists to delete");
       deleteConductorRepository.deleteById(id);
    }
}
