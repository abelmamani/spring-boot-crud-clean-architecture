package usecases;

import exceptions.ConductorNotExistException;
import inputs.GetConductorInput;
import models.Conductor;
import outputs.GetConductorRepository;

import java.util.Optional;

public class GetConductorUseCase implements GetConductorInput {
    private GetConductorRepository getConductorRepository;

    public GetConductorUseCase(GetConductorRepository getConductorRepository) {
        this.getConductorRepository = getConductorRepository;
    }

    @Override
    public Conductor getConductor(Long id) {
        Optional<Conductor> conductor = getConductorRepository.findById(id);
        if(conductor.isEmpty())
            throw new ConductorNotExistException("the conductor by id not exist");
        return conductor.get();
    }
}
