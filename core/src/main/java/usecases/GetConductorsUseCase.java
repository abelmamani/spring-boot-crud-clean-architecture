package usecases;

import inputs.GetConductorsInput;
import models.Conductor;
import outputs.GetConductorsRepository;

import java.util.Collection;

public class GetConductorsUseCase implements GetConductorsInput {
    private GetConductorsRepository getConductorsRepository;

    public GetConductorsUseCase(GetConductorsRepository getConductorsRepository) {
        this.getConductorsRepository = getConductorsRepository;
    }

    @Override
    public Collection<Conductor> getConductors() {
        return getConductorsRepository.findAll();
    }
}
