package usecases;

import exceptions.ConductorAlreadyExistsException;
import inputs.GetConductorInput;
import inputs.UpdateconductorInput;
import models.Conductor;
import models.UpdateConductorRequestModel;
import outputs.UpdateConductorRepository;

public class UpdateConductorUseCase implements UpdateconductorInput {
    private UpdateConductorRepository updateConductorRepository;
    private GetConductorInput getConductorInput;

    public UpdateConductorUseCase(UpdateConductorRepository updateConductorRepository, GetConductorInput getConductorInput) {
        this.updateConductorRepository = updateConductorRepository;
        this.getConductorInput = getConductorInput;
    }

    @Override
    public Conductor updateConductor(UpdateConductorRequestModel updateConductorRequestModel) {
        Conductor conductor = getConductorInput.getConductor(updateConductorRequestModel.getId());
        if(updateConductorRepository.existsByDni(updateConductorRequestModel.getDni()) && !conductor.getDni().equals(updateConductorRequestModel.getDni()))
            throw new ConductorAlreadyExistsException("the conductor with dni already exists");
        Conductor newConductor = Conductor.getInstance(updateConductorRequestModel.getId(),
                updateConductorRequestModel.getDni(),
                updateConductorRequestModel.getName(),
                updateConductorRequestModel.getLastName(),
                updateConductorRequestModel.getHireDate());
        return updateConductorRepository.update(newConductor);
    }
}
